const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");
const mongoose = require("mongoose");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) =>{
	let input = request.body;
	const userData = auth.decode(request.headers.authorization);

		if(userData.isAdmin) {let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});
	
		// saves the created object to our database
		return newCourse.save()
		// course succesfully created
		.then(course =>{
			console.log(course);
			response.send(course);
		})
		// course creation failed
		.catch(error =>{
			console.log(error);
			response.send(false);
		})}
		else {
			return response.send("You are not an admin.");
		}
}